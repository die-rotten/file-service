<?php

use App\File;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;

class FileControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $storage;

    protected $file_system;

    protected $file;

    public function setUp(): void
    {
        parent::setUp();

        $this->file_system = Mockery::mock('Illuminate\Filesystem\Filesystem');
        $this->file = Mockery::mock('Illuminate\Contracts\Filesystem\Filesystem');
        $this->storage = Mockery::mock('Illuminate\Contracts\Filesystem\Factory');
    }

    public function testUpload()
    {
        $uploaded_file = Mockery::mock(
            'Illuminate\Http\UploadedFile',
            [
                'getClientOriginalName'      => 'image-1.jpg',
                'getClientOriginalExtension' => 'jpg',
                'getClientMimeType' => 'img/jpg',
                'storeAs' => '/images',
                'getPath' => '/path/to/file',
                'isValid' => true,
                'guessExtension' => 'jpg',
                'getRealPath' => null,
            ]
        );

        $this->file_system->shouldReceive('get')->andReturn(true);

        $disk = Mockery::mock('\Illuminate\Contracts\Filesystem\Filesystem', ['put' => null]);
        $this->storage->shouldReceive('disk')->andReturn($disk);

        $files = [
            'file' => $uploaded_file
        ];

        $this->call('POST', '/files', [], [], $files);

        $files = File::get();

        $this->assertResponseStatus(200);
        $this->seeJsonEquals([
            'name' => $files[0]->name,
            'type' => $files[0]->type,
            'extension' => $files[0]->extension,
        ]);
    }
}
