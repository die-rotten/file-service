<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FileController extends Controller
{
    public function upload(Request $request)
    {
        $this->validate($request, [
            'file' => 'required',
        ]);

        try {
            $extension = $request->file('file')->getClientOriginalExtension();
            $type = $request->file('file')->getClientMimeType();
            $name = (string) Str::uuid();
            $filename = $name . '.' . $extension;
            $request->file('file')->storeAs('/images', $filename, ['s3', 'public']);

            $data = [
                'name' => $filename,
                'type' => $type,
                'extension' => $extension,
            ];

            $file = File::create($data);

            return response()->json([
                'name' => $file->name,
                'type' => $file->type,
                'extension' => $file->extension,
            ]);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
