FROM composer AS build

WORKDIR /build

ADD . .
RUN composer install --no-dev --ignore-platform-reqs

FROM webdevops/php-nginx:alpine

WORKDIR /app

COPY --from=build /build .

ENV WEB_DOCUMENT_ROOT /app/public
